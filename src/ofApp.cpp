#include "ofApp.h"

using namespace ofxLaser;


//--------------------------------------------------------------
void ofApp::setup(){
    ofSetEscapeQuitsApp(false);
    laserWidth = 800; // FX
    laserHeight = 800;

    videoGetDevices();
/*
    // SEQUENCER
    sequencer.setup(16,120,4);
    //p1.set("param 1 (i)", 2, 0, 4);
    p1.set("zone 1 solo", false);
    p2.set("zone 2 solo", false);
    p3.set("zone 3 solo", false);
    p4.set("zone 4 solo", false);
    //p5.set("zone 5 solo", false);
    //p6.set("zone 6 solo", false);

    //ofLog() << "p1 name" << p1.getName();
    sequencer.addRow(&p1);
    sequencer.addRow(&p2);
    sequencer.addRow(&p3);
    sequencer.addRow(&p4);
   // sequencer.addRow(&p5);
 //   sequencer.addRow(&p6);

    //sequencer.randomize();
    //sequencer.setSmooth(true);
    //sequencer.start();
    //sequencer.setPosition(200,990,600,100);


    sequencer.setPosition(ofGetWindowWidth()-1050,ofGetWindowHeight()-50,900,100);
*/


    // MIDI
    // open port by number (you may need to change this)
    midiIn.openPort(0);
    midiIn.ignoreTypes(false, // sysex  <-- don't ignore timecode messages!
                       false, // timing <-- don't ignore clock messages!
                       true); // sensing

    // add ofApp as a listenera
    midiIn.addListener(this);

    // LASER
    // get the filenames of all the svgs in the data/svgs folder
    string defaultPath = "svgs/";
    ofDirectory dir(defaultPath);
    dir.allowExt("svg");
    dir.listDir();
    dir.sort();
    // and load them all
    const vector<ofFile>& files = dir.getFiles();
    dir.close();

    //svgs.resize(files.size());

    for(int i = 0; i<files.size();i++) {
        const ofFile & file = files.at(i);
		laserGraphics.emplace_back();
        // addSvgFromFile(string filename, bool optimise, bool subtractFills)
		laserGraphics.back().addSvgFromFile(file.getAbsolutePath(), false, true);

		// this centres all the graphics
        laserGraphics.back().autoCentre();
		ofLog(OF_LOG_NOTICE,file.getAbsolutePath());
		fileNames.push_back(file.getFileName());
    }


    currentSVG.set("Current SVG", 0, 0, laserGraphics.size()-1);
    currentSVGFilename.set("Filename");
    svgScale.set("SVG scale", 1.0, 0.1,6);
    //midi settings
    svgFollowBeat.set("Sequence SVG",false);
    seqSolo.set("Sequence solo",false);
    beatIntervalSpeedIndex.set("Speed",5,1,8);

    //svg settings
    rotate3D.set("Rotate 3D", true);
    rotateX.set("Rotate X", false);
    rotateY.set("Rotate Y", false);
    rotateZ.set("Rotate Z", false);
    rotateSpeed.set("Rotation speed",30,1,127);

    // fx settings
    currentLaserEffect.set("Current Effect", 1, 1, numLaserEffects-1);
    fxLoadDefaults.set("Set defaults on load", true);

    fxRed.set("Red",0,0,255);
    fxGreen.set("Green",0,0,255);
    fxBlue.set("Blue",0,0,255);
    fxParamA.set("Parameter A",0,1,100);
    fxParamB.set("Parameter B",0,1,100);
    fxParamC.set("Parameter C",0,1,100);
    fxParamD.set("Parameter D",0,1,100);
    fxParamE.set("Parameter E",0,1,100);

    //Sequencer
    currentSolo.set("Current solo",0, 0, laserManager.getNumZones());
    svgEnabled.set("Enable SVG", false);
    fxEnabled.set("Enable FX", false);
    seqBPM.set("Internal BPM",160,10,480);


    //Video input
    videoEnabled.set("Enable Video input", false);
    videoRed.set("Red",0,0,255);
    videoGreen.set("Green",0,0,255);
    videoBlue.set("Blue",0,0,255);
    //contourThreshold.set("Threshhold", 20, 1, 256);
    //contourSimplify.set("Simplify", 0.5, 0.1, 10.0);
    //contourResample.set("Resample", 10, 1, 100);


    //custom settings
    laserManager.addCustomParameter(renderProfileLabel.set("Render Profile name",""));
    laserManager.addCustomParameter(renderProfileIndex.set("Render Profile", 2, 0, 2));




    //laserManager.addCustomParameter(currentLaserEffect.set("Current effect", 0, 0, numLaserEffects-1));

    //ofParameter<string> description;
    //description.set("description", "INSTRUCTIONS : \nLeft and Right Arrows to change current SVG \nTAB to toggle output editor \nF to toggle full screen");
    //laserManager.addCustomParameter(description);

    oscRecv.setup(oscRecvPort);
    oscSend.setup(oscSendHost, oscSendPort);

    //NDI RECEIVER

    //ndiTexture.allocate(640, 480, GL_RGBA);


}

//--------------------------------------------------------------
void ofApp::exit(){
    ofLog() << "quiting.. bye!";
    contourResetScheduler.waitForThread();
    midiIn.closePort();
    midiIn.removeListener(this);
    videoCleanup();


}


void ofApp::update(){
   // resetContour();
    //ndiReceiver.ReceiveImage(ndiTexture);
    sequencer.setBpm(seqBPM);
    //videoUpdate();
    test_videoUpdate();


    //float deltaTime = ofClamp(ofGetLastFrameTime(), 0, 0.2);
    //elapsedTime += (deltaTime*timeSpeed/1000);
    elapsedTime = ofGetElapsedTimef();
    laserManager.update();
    //osc
    oscReceiveMessages();

    fxParamA.setName(fxParamALabel);
    fxParamB.setName(fxParamBLabel);
    fxParamC.setName(fxParamCLabel);
    fxParamD.setName(fxParamDLabel);
    fxParamE.setName(fxParamELabel);


}
/*
void ofApp::updateSequencerView(){
    for(int i=0 ; i<4; i++){
        struct zoneSequence seq = {true, i};
        seq.seqSolo.set("bla",false);
        zoneSequences.push_back(seq);

        sequencer.addRow(&seq->seqSolo);
    }
}
*/

void ofApp::movePolyline(ofPolyline *polyLine, int x, int y){
    ofLog() << "move polyline to x: "<< x << " y: " << y;

}

void ofApp::updateSequencerView(){
    //ofLog() << "update sequencer view";
    std::vector<ofxLaser::Laser*> lasers;
    lasers = laserManager.getLasers();
    int selectedLaser = laserManager.getSelectedLaser();
    if(selectedLaser!=-1)  {
        ofxLaser::Laser* laser = lasers[selectedLaser];
        laser->getActiveZones();
        for(ofxLaser::LaserZone* laserZone : laser->laserZones) {
            zoneSequences.push_back(zoneSequence());
            zoneSequences[laserZone->getZoneIndex()].seqSolo.set("laserZone->getLabel()",false);
            //ofLog() << "laserzone: " << zoneSequences[laserZone->getZoneIndex()]->seqSolo.getName() << " is enabled:" << laserZone->getEnabled();
            sequencer.addRow(&zoneSequences[laserZone->getZoneIndex()].seqSolo);
            //sequencer.addRow(&p1);
            //laserZone->soloed = false;
            //if(laserZone->getZoneIndex() == currentSolo){
            //if(laserZone->getZoneIndex() == 0){
            //    if(p1.get() == true){
            //        laserZone->soloed = true;
            //    }
            //}

        }
    }
}

void ofApp::soloZone(int zoneNumber, bool solo){
  std::vector<ofxLaser::Laser*> lasers;
  lasers = laserManager.getLasers();
  int selectedLaser = laserManager.getSelectedLaser();
  if(selectedLaser!=-1 )  {
      ofxLaser::Laser* laser = lasers[selectedLaser];
      laser->getActiveZones();
      for(ofxLaser::LaserZone* laserZone : laser->laserZones) {
          ofLog() << "laserzone: " << laserZone->getZoneIndex() << "is enabled:" << laserZone->getEnabled();
          laserZone->soloed = false;
          if(laserZone->getZoneIndex() == zoneNumber && solo == true ) laserZone->soloed = true;
      }
  }

}

void ofApp::draw() {
    //ndiTexture.draw(0, 0);

	ofBackground(15,15,20);
    sequencer.draw();


    string renderProfile;
    switch (renderProfileIndex) {
        case 0 :
            renderProfile = OFXLASER_PROFILE_DEFAULT;
            break;
        case 1 :
            renderProfile = OFXLASER_PROFILE_DETAIL;
            break;
        case 2 :
            renderProfile = OFXLASER_PROFILE_FAST;
            break;
    }
    renderProfileLabel = "Render Profile : OFXLASER_PROFILE_" + renderProfile;


    if (fileNames.size() > 0){
            currentSVGFilename = fileNames[currentSVG];
    }

    laserManager.beginDraw();
    ofPushMatrix();

    ofTranslate(400, 400);
    ofScale(svgScale, svgScale);
    if(rotate3D) {
        float angle = fmod(ofGetElapsedTimef()*rotateSpeed*3, 180)-90;
        if(rotateX)
            ofRotateXDeg(angle);
        if(rotateY)
            ofRotateYDeg(angle);
        if(rotateZ)
            ofRotateZDeg(angle);
    }
    if(laserGraphics.size()>currentSVG) {
        if(svgEnabled){
            laserManager.drawLaserGraphic(laserGraphics[currentSVG], 1, renderProfile);
        }

    }
    ofPopMatrix();

    laserManager.endDraw();
    if (fxEnabled){
        showLaserEffect(currentLaserEffect);
    }

    //VIDEO INPUT
    /*
    if(videoEnabled){
        ofColor c;
        c.r=0;
        c.g=0;
        c.b=255;
        //ofLog() << "size of videographics" << videoGraphics.size();
        laserManager.drawLaserGraphic(videoGraphic,1,renderProfile);
        videoGraphic.clear();


        //laserManager.drawPoly(cvPoly,c,renderProfile);
    }
*/
    ofColor c;
    c.r=0;
    c.g=0;
    c.b=255;
    for (int i = 0 ; i < numVideoInputs ; i++){
        if (videoInputs[i].videoEnabled == true){
            videoInputs[i].colorImage.draw(ofGetScreenWidth()-330,ofGetScreenHeight()-285,320,240);
            videoInputs[i].contour.draw(ofGetScreenWidth()-330,ofGetScreenHeight()-285,320,240);
            if(videoEnabled){

                //ofLog() << "size of videographics" << videoGraphics.size();
                laserManager.drawLaserGraphic(videoGraphic,1,renderProfile);
                //videoGraphic.clear();
            }
            //laserManager.drawPoly(videoInputs[i].cvPoly,c,renderProfile);
        }
    }

    laserManager.send();
    // GUI
    //laserManager.drawUI();
    laserManager.startLaserUI();
    // draw all the input and output previews, including the
    // laser transformation system
    laserManager.drawPreviews();

    // draw all the laser UI panels
    buildGui();

    //SEQ Logic
    //ofLog() << "beatcount: " << beatCount;


/*
    if(beatCount % beatIntervalSpeed == 0 && svgFollowBeat == true ){
        currentSVG++;
    }
    if(currentSVG>=laserGraphics.size()) currentSVG = 0;
*/
    std::vector<ofxLaser::Laser*> lasers;
    lasers = laserManager.getLasers();
    int selectedLaser = laserManager.getSelectedLaser();
    if(selectedLaser!=-1 && seqSolo )  {
        ofxLaser::Laser* laser = lasers[selectedLaser];
        laser->getActiveZones();
        for(ofxLaser::LaserZone* laserZone : laser->laserZones) {
            //ofLog() << "laserzone: " << laserZone->getZoneIndex() << "is enabled:" << laserZone->getEnabled();
            laserZone->soloed = false;
            //if(laserZone->getZoneIndex() == currentSolo){
            //if(zoneSequences[laserZone->getZoneIndex()].seqSolo.get() == true){
            //    laserZone->soloed = true;
            //}
            if(laserZone->getZoneIndex() == 0){
                if(p1.get() == true){
                    laserZone->soloed = true;
                }
            }
            if(laserZone->getZoneIndex() == 1){
                if(p2.get() == true){
                    laserZone->soloed = true;
                    resetContour();
                }
            }
            if(laserZone->getZoneIndex() == 2){
                if(p3.get() == true){
                    laserZone->soloed = true;
                }
            }
            if(laserZone->getZoneIndex() == 3){
                if(p4.get() == true){
                    laserZone->soloed = true;
                }
            }/*
            if(laserZone->getZoneIndex() == 4){
                if(p5.get() == true){
                    laserZone->soloed = true;
                }
            }
            if(laserZone->getZoneIndex() == 5){
                if(p6.get() == true){
                    laserZone->soloed = true;
                }
            }
*/
        }
    }

//    if(videoEnabled){
//        colorImage.draw(ofGetScreenWidth()-330,ofGetScreenHeight()-285,320,240);
//        contour.draw(ofGetScreenWidth()-330,ofGetScreenHeight()-285,320,240);
//    }


    //ofLog() << "zoneindex" << numZones;


}




//--------------------------------------------------------------
void ofApp::keyPressed(int key){


   if(key =='f') {
        ofToggleFullscreen();
    } else if (key == OF_KEY_LEFT) {
		currentSVG--;
		if(currentSVG<0) currentSVG = laserGraphics.size()-1;
	} else if (key == OF_KEY_RIGHT) {
		currentSVG++;
		if(currentSVG>=laserGraphics.size()) currentSVG = 0;
	}

    if(key==OF_KEY_TAB) laserManager.selectNextLaser();

}

void ofApp::newMidiMessage(ofxMidiMessage& message) {

    // MIDI CLOCK




    // update the clock length and song pos in beats
    if(clock.update(message.bytes)) {
        // we got a new song pos
        beats = clock.getBeats();

        seconds = clock.getSeconds();

    }

    // compute the seconds and bpm
    switch(message.status) {

        // compute seconds and bpm live, you may or may not always need this
        // which is why it is not integrated into the ofxMidiClock parser class
        case MIDI_TIME_CLOCK:
            beatCount++;
            if(beatCount % beatIntervalSpeed == 0 && svgFollowBeat == true ) {
                currentSVG++;
                if(currentSVG>=laserGraphics.size()) currentSVG = 0;
            }
            if(beatCount % beatIntervalSpeed == 0 && seqSolo == true ){
                currentSolo++;
                if(currentSolo>=laserManager.getNumZones()) currentSolo = 0;
            }

            seconds = clock.getSeconds();
            bpm += (clock.getBpm() - bpm) / 5; // average the last 5 bpm values
            // no break here so the next case statement is checked,
            // this way we can set clockRunning if we've missed a MIDI_START
            // ie. master was running before we started this example

        // transport control
        case MIDI_START: case MIDI_CONTINUE:
            if(!clockRunning) {
                clockRunning = true;
                ofLog() << "clock started";
            }
            break;
        case MIDI_STOP:
            if(clockRunning) {
                clockRunning = false;
                ofLog() << "clock stopped";
            }
            break;

        default:
            break;
    }

    // MIDI TIMECODE
/*
    // update the timecode pos
    if(timecode.update(message.bytes)) {

        // we got a new frame pos
        frame = timecode.getFrame();

        // if the last message was a timecode quarter frame message,
        // then assume the timecode master has started playback
        if(message.status == MIDI_TIME_CODE) {
            if(!timecodeRunning) {
                timecodeRunning = true;
                ofLog() << "timecode started";
            }
            timecodeTimestamp = ofGetElapsedTimeMillis();
        }
    }
    */
}
