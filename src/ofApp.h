#pragma once

#define USE_ETHERDREAM
//#define USE_IDN
//#define USE_LASERDOCK
#include <cassert>
//#include "fMain.h"
#include "ofxLaserManager.h"
#include "ofxLaserGraphic.h"
#include "ofxMidi.h"
#include "ofxMidiClock.h"
#include "ofxMidiTimecode.h"
#include "ofxOsc.h"
#include "ofxOpenCv.h"
//#include "ofxNDI.h"
#include "ofxSequencer.h"

class Scheduler : public ofThread {
public:
    bool reset = false;

    Scheduler() {
        timer.setPeriodicEvent(1000000000); // 1 second
        startThread();
    }
private:
    ofTimer timer;
    void threadedFunction() {
        while (isThreadRunning()) {
            timer.waitNext();
            reset = true;
        }
    }
};



class ofApp : public ofBaseApp, public ofxMidiListener{
public:
	void setup();
	void update();
	void draw();
    void exit();

	void keyPressed  (int key);

    //helper functions
    void movePolyline(ofPolyline *polyLine, int x, int y);
    //ndi
    //ofxNDIreceiver ndiReceiver;
    //ofTexture ndiTexture;


    ofParameter<int> currentSVG;
    ofParameter<string> currentSVGFilename;
    ofParameter<bool> svgFollowBeat;
    ofParameter<float> svgScale;
    ofParameter<bool> rotate3D;
    ofParameter<bool> rotateX;
    ofParameter<bool> rotateY;
    ofParameter<bool> rotateZ;
    ofParameter<int> rotateSpeed;
    ofParameter<bool> svgEnabled;
    ofParameter<bool> fxEnabled;
    ofParameter<int> renderProfileIndex;
    ofParameter<string> renderProfileLabel;
    ofParameter<int> beatIntervalSpeedIndex;
    ofParameter<int> beatIntervalSpeed;
    ofParameter<string> beatIntervalSpeedLabel;

  	vector<ofxLaser::Graphic> laserGraphics;
    vector<string> fileNames;
    vector<string> svgDirs;

    //Sequencer
    void updateSequencerView();
    struct zoneSequence {
        ofParameter<bool>  seqSolo;
        ofParameter<int> zoneNum;
    };
    //zoneSequence zoneSequences;
    vector<zoneSequence> zoneSequences;
    ofxSequencer sequencer;
    ofParameter<bool> p1;
    ofParameter<bool> p2;
    ofParameter<bool> p3;
    ofParameter<bool> p4;
    ofParameter<bool> p5;
    ofParameter<bool> p6;

    ofParameter<int> seqBPM;

    // video input and opencv
    int videoStart();
    int videoStop();
    void videoUpdate();
    void test_videoUpdate();
    int videoGetDevices();
    void drawVideoInputs();
    void resetContour();
    int videoCleanup();
    vector<ofVideoDevice> videoDevices;
    vector<string> videoDeviceNames;
    int selectedVideoDev;
    ofParameter<bool> videoEnabled;
    ofVideoGrabber vidGrabber;
    ofPixels videoInverted;
    ofTexture videoTexture;
    int videoWidth;
    int videoHeight;
    ofxCvColorImage colorImage;
    ofxCvGrayscaleImage grayImage;
    ofxCvGrayscaleImage background;
    ofxCvGrayscaleImage difference;
    ofxCvContourFinder contour;
    ofParameter<int> contourThreshold;
    ofParameter<float> contourSimplify;
    ofParameter<float> contourResample;
    bool learn = true;
    ofPolyline cvPoly;
    ofPolyline cvCur;

    int numVideoInputs = 0;

    int videoInputAdd(int deviceNum);
    struct videoInput {
        ofParameter<string> label;
        ofParameter<int> videoDevNum;
        ofVideoGrabber grabber;
        ofParameter<bool> videoEnabled = false;
        ofPixels videoInverted;
        ofTexture videoTexture;
        int videoWidth;
        int videoHeight;
        ofxCvColorImage colorImage;
        ofxCvGrayscaleImage grayImage;
        ofxCvGrayscaleImage background;
        ofxCvGrayscaleImage difference;
        ofxCvContourFinder contour;
        ofParameter<int> contourThreshold = 120;
        ofParameter<float> contourSimplify = 0.5;
        ofParameter<float> contourResample = 10;
        ofParameter<float> rotateDeg;
        bool learn = true;
        ofPolyline cvPoly;
        ofPolyline cvCur;
    };
    ofxLaser::Graphic videoGraphic ;
    vector<videoInput> videoInputs;
    ofParameter<int> videoRed;
    ofParameter<int> videoGreen;
    ofParameter<int> videoBlue;



    //laserFX
    void showLaserEffect(int effectnum);

    ofParameter<bool> fxLoadDefaults;
    ofParameter<bool> fxJustloaded = false;
    ofParameter<int> currentLaserEffect;
    ofParameter<float>timeSpeed;
    int numLaserEffects = 9;
    float elapsedTime;
    std::vector<ofPolyline> polyLines;
    ofParameter<ofColor> colour;
    string fxName;
    ofParameter<int> fxRed;
    ofParameter<int> fxGreen;
    ofParameter<int> fxBlue;
    ofParameter<float> fxParamA;
    ofParameter<float> fxParamB;
    ofParameter<float> fxParamC;
    ofParameter<float> fxParamD;
    ofParameter<float> fxParamE;
    ofParameter<string> fxParamALabel;
    ofParameter<string> fxParamBLabel;
    ofParameter<string> fxParamCLabel;
    ofParameter<string> fxParamDLabel;
    ofParameter<string> fxParamELabel;

    //Sequencer
    ofParameter<bool> seqSolo;
    ofParameter<int> currentSolo;

   // ofParameter<int> currentMute;
   // int numZones;


    // Manager
    ofxLaser::Manager laserManager;
    ofxLaser::Graphic& laserGraphic = laserGraphics[currentSVG];

    // GUI
    void buildGui();
    string intervalSpeedString;

    int laserWidth;
    int laserHeight;

    ofxMidiIn midiIn;
    bool verbose = false;

    // MIDI CLOCK
    void newMidiMessage(ofxMidiMessage& eventArgs);
    ofxMidiClock clock; //< clock message parser
    bool clockRunning = false; //< is the clock sync running?
    unsigned int beats = 0; //< song pos in beats
    double seconds = 0; //< song pos in seconds, computed from beats
    double bpm = 120; //< song tempo in bpm, computed from clock length
    unsigned int beatCount = 0;

    //
    void oscReceiveMessages();
    void oscSendAllControls();
		void soloZone(int zoneNumber, bool solo);

    int oscRecvPort = 9997;
    int oscSendPort = 9996;
    string oscSendHost= "localhost";
    ofxOscReceiver oscRecv;
    ofxOscSender oscSend;


    int32_t oscFxRed;
    int32_t oscFxGreen;
    int32_t oscFxBlue;
    int32_t oscFxParamA;
    int32_t oscFxParamB;
    int32_t oscFxParamC;
    int32_t oscFxParamD;
    int32_t oscFxParamE;
		Scheduler contourResetScheduler;

};
