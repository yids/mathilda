 #include "ofApp.h"

void ofApp :: showLaserEffect(int effectnum) {
    string renderProfile;
    switch (renderProfileIndex) {
    case 0 :
        renderProfile = OFXLASER_PROFILE_DEFAULT;
        break;
    case 1 :
        renderProfile = OFXLASER_PROFILE_DETAIL;
        break;
    case 2 :
        renderProfile = OFXLASER_PROFILE_FAST;
        break;
    }
    renderProfileLabel = "Render Profile : OFXLASER_PROFILE_" + renderProfile;


    float left = laserWidth*0.1;
    float top = laserHeight*0.1;
    float right = laserWidth*0.9;
    float bottom = laserHeight*0.9;
    float width = laserWidth*0.8;
    float height = laserHeight*0.8;

    switch (currentLaserEffect) {

    case 1: {    
        // LASER LINES
        fxName = "Scanning lines";
        fxParamALabel = "NumLines"; //def = 7
        fxParamBLabel = "Scan Speed";
        fxParamCLabel = "None";
        fxParamDLabel = "None";
        fxParamELabel = "None";
        if (fxLoadDefaults == true && fxJustloaded == true){
            fxParamA = 64;
            fxParamB = 50;
            fxParamC = 0;
            fxParamD = 0;
            fxParamE = 0;
            fxJustloaded = false;
        }
        int numlines = fxParamA/8;
        for(int i = 0; i<numlines; i++) {
            float progress =(float)i/(float)(numlines-1);
            //float hue =(float)i/(float)(numlines);
            //float hue = 1.4;
            float xpos =tan(elapsedTime*fxParamB/50.0)*left + (width*progress);
            ofColor c;
            c.r=fxRed;
            c.g=fxGreen;
            c.b=fxBlue;
            laserManager.drawLine(ofPoint(xpos, top+height*0.1), ofPoint(xpos, top+height*0.9), c, renderProfile);

        }

        break;

    }


    case 2: {
        fxName = "Rave lines";
        fxParamALabel = "NumLines";
        fxParamBLabel = "None";
        fxParamCLabel = "None";
        fxParamDLabel = "None";
        fxParamELabel = "None";
        if (fxLoadDefaults == true && fxJustloaded == true){
            fxParamA = 128;
            fxParamB = 0;
            fxParamC = 0;
            fxParamD = 0;
            fxParamE = 0;
            fxJustloaded = false;
        }
        int numlines = fxParamA/8;
        for(int i = 0; i<numlines; i++) {
            float progress =(float)i/(float)(numlines-1);
            //float hue =(float)i/(float)(numlines);
            float hue =85;
            float xpos =left*10 + (width*progress) + (sin(elapsedTime*2+i*0.5)*width*0.05);
            ofColor c;
            //c.setHsb(hue*255, 255, 255);
            c.r=fxRed;
            c.g=fxGreen*(i*10);
            c.b=fxBlue;
            laserManager.drawLine(ofPoint(xpos*sin((elapsedTime/50)*i), top+height*0.1), ofPoint(xpos*cos((elapsedTime/5)*i), top+height*0.9), c, renderProfile);
        }
        break;
    }


    case 3: {
        // LASER CIRCLES
        fxName = "Audi Cirles";
        fxParamALabel = "NumCircles"; //def = 7
        fxParamBLabel = "Pulse Speed"; // def
        fxParamCLabel = "None";
        fxParamDLabel = "None";
        fxParamELabel = "None";
        if (fxLoadDefaults == true && fxJustloaded == true){
            fxParamA = 64;
            fxParamB = 16;
            fxParamC = 0;
            fxParamD = 0;
            fxParamE = 0;
            fxJustloaded = false;
        }
        int numCircles = fxParamA/16;
        for(int i = 0; i<numCircles; i++) {
            float progress =(float)i/(float)(numCircles-1);
            float xpos =left + (width*progress);
            ofColor c;
            c.r=fxRed;
            c.g=fxGreen;
            c.b=fxBlue;
            laserManager.drawCircle(ofPoint(xpos, top+height/2), 40*sin(elapsedTime*(fxParamB)), c, renderProfile);
        }

        break;

    }

    case 4: {
        // LASER CIRCLES ANIMATING
        fxName = "Wave Cirles";
        fxParamALabel = "NumCircles";
        fxParamBLabel = "Movement Speed";
        fxParamCLabel = "Mod Size";
        fxParamDLabel = "Mod Speed";
        fxParamELabel = "None";
        if (fxLoadDefaults == true && fxJustloaded == true){
            fxParamA = 40.0;
            fxParamB = 10.0;
            fxParamC = 10.0;
            fxParamD = 20.0;
            fxParamE = 0;
            fxJustloaded = false;
        }
        int numCircles = fxParamA/5;

        for(int i = 0; i<numCircles; i++) {

            float progress =(float)i/(float)(numCircles-1);
            float hue =(float)i/(float)(numCircles);

            float xpos =left + (width*progress) + (sin(elapsedTime*4+i*0.5)*width*0.05);
            float ypos =top + (height*progress) + (sin(elapsedTime*4+i*0.5)*width*0.05);

            //laserManager.drawCircle(ofPoint(xpos, top+height*0.3), 30, ofColor::white, renderProfile);
            ofColor c;
            c.r=fxRed;
            c.g=fxGreen;
            c.b=fxBlue;
            laserManager.drawCircle(ofPoint(xpos, top+(height*0.5)/(sin((elapsedTime*i)*(fxParamB/100))*4)+300), sin(elapsedTime*(fxParamD/10))*fxParamC, c, renderProfile);

        }

        break;

    }

    case 5: {
        // LASER PARTICLES
        fxName = "Particles";
        fxParamALabel = "None";
        fxParamBLabel = "None";
        fxParamCLabel = "None";
        fxParamDLabel = "None";
        fxParamELabel = "None";
        if (fxLoadDefaults == true && fxJustloaded == true){
            fxParamA = 0;
            fxParamB = 0;
            fxParamC = 0;
            fxParamD = 0;
            fxParamE = 0;
            fxJustloaded = false;
        }
        int numParticles = 12;

        for(int i = 0; i<numParticles; i++) {

            float progress =(float)i/(float)(numParticles-1);

            float xpos =left + (width*progress) ;

            laserManager.drawDot(ofPoint(xpos, top+height*0.3), ofColor(255),1, renderProfile);
            ofColor c;
            c.setHsb(progress*255, 255, 255);
            laserManager.drawDot(ofPoint(xpos, top+height*0.7), c, 1,  renderProfile);

        }

        break;

    }
    case 6: {
        // LASER PARTICLES ANIMATING
        fxName = "More particles";
        fxParamALabel = "None";
        fxParamBLabel = "None";
        fxParamCLabel = "None";
        fxParamDLabel = "None";
        fxParamELabel = "None";
        if (fxLoadDefaults == true && fxJustloaded == true){
            fxParamA = 0;
            fxParamB = 0;
            fxParamC = 0;
            fxParamD = 0;
            fxParamE = 0;
            fxJustloaded = false;
        }
        float speed = 1;
        for(int i = 0; i<30; i++) {

            ofColor c;
            c.setHsb(i*6,255,255);
            ofPoint p;
            float spread = ofMap(cos(elapsedTime*0.4),1,-1,0.01,0.1);
            p.x = sin((elapsedTime-((float)i*spread)) *1.83f * speed) * 300;
            p.y = sin((elapsedTime-((float)i*spread)) *2.71f *speed) * 300;
            p.x+=laserWidth/2;
            p.y+=laserHeight/2;

            laserManager.drawDot(p, c, 1, renderProfile);

        }

        break;

    }
    case 7: {
        // Platonic
        // 3D rotation
        // you don't need to wrap your draw calls in
        // laser.beginDraw() and laser.endDraw() unless
        // you're doing 3D (it fixes the viewport perspective)
        fxName = "Platonic";
        fxParamALabel = "None";
        fxParamBLabel = "None";
        fxParamCLabel = "None";
        fxParamDLabel = "None";
        fxParamELabel = "None";
        if (fxLoadDefaults == true && fxJustloaded == true){
            fxParamA = 0;
            fxParamB = 0;
            fxParamC = 0;
            fxParamD = 0;
            fxParamE = 0;
            fxJustloaded = false;
        }
        laserManager.beginDraw();

        float speed = 20;
        ofPushMatrix();
        ofTranslate(laserWidth/2,laserHeight/2);
        ofRotateYDeg(elapsedTime*speed);
        int hue = (int)(elapsedTime*32)%255; // 8 seconds to go around
        ofColor c;
        c.setHsb(hue, 255, 255);

        ofPolyline poly;

        for(int j = 0; j<5; j++) {
            poly.clear();
            ofPushMatrix();
            ofRotateXDeg(j*90);

            // CUBE
            poly.addVertex(glm::vec3(100,-100,100));
            poly.addVertex(glm::vec3(100, 100,100));
            poly.addVertex(glm::vec3(-100, 100,100));
            poly.addVertex(glm::vec3(-100, -100,100));

            // ICOSAHEDRON
/*
            poly.addVertex(glm::vec3(100,0,0));
            poly.addVertex(glm::vec3(0, 100,0));
            poly.addVertex(glm::vec3(0, 0,100));
            poly.addVertex(glm::vec3(-100, 0,0));
            poly.addVertex(glm::vec3(0, -100,0));
            poly.addVertex(glm::vec3(0, 0,-100));
*/

            laserManager.drawPoly(poly, c, renderProfile);
            ofPopMatrix();
        }
        ofPopMatrix();
        laserManager.endDraw();
        break;
    }


    }

    // LASER POLYLINES
    for(size_t i = 0; i<polyLines.size(); i++) {
        laserManager.drawPoly(polyLines[i], colour, renderProfile );
    }

}
