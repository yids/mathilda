#include "ofApp.h"


void ofApp::oscReceiveMessages()
{
    while(oscRecv.hasWaitingMessages()){
        ofxOscMessage m;
        oscRecv.getNextMessage(&m);
        if(m.getAddress() == "/fx/red"){oscFxRed = m.getArgAsInt32(0);fxRed=oscFxRed;}
        if(m.getAddress() == "/fx/green"){oscFxGreen = m.getArgAsInt32(0);fxGreen=oscFxGreen;}
        if(m.getAddress() == "/fx/blue"){oscFxBlue = m.getArgAsInt32(0);fxBlue=oscFxBlue;}
        if(m.getAddress() == "/fx/a")(fxParamA=m.getArgAsFloat(0));
        if(m.getAddress() == "/fx/b")(fxParamB=m.getArgAsFloat(0));
        if(m.getAddress() == "/fx/c")(fxParamC=m.getArgAsFloat(0));
        if(m.getAddress() == "/fx/d")(fxParamD=m.getArgAsFloat(0));
        if(m.getAddress() == "/fx/e")(fxParamE=m.getArgAsFloat(0));
        if(m.getAddress() == "/util/globalBrightness")( laserManager.globalBrightness=m.getArgAsInt(0));
        if(m.getAddress() == "/util/fxOn")( fxEnabled=m.getArgAsBool(0));
        if(m.getAddress() == "/util/svgOn")( svgEnabled=m.getArgAsBool(0));
        if(m.getAddress() == "/util/videoOn")( videoEnabled=m.getArgAsBool(0));
        if(m.getAddress() == "/util/seqSvgOn")( svgFollowBeat=m.getArgAsBool(0));
        if(m.getAddress() == "/util/seqSoloOn")( seqSolo=m.getArgAsBool(0));
        if(m.getAddress() == "/video/contour/threshold" && videoEnabled) videoInputs[0].contourThreshold=m.getArgAsInt(0);
        if(m.getAddress() == "/video/color/red" && videoEnabled) videoRed=m.getArgAsInt(0);
        if(m.getAddress() == "/video/color/green" && videoEnabled) videoGreen=m.getArgAsInt(0);
        if(m.getAddress() == "/video/color/blue" && videoEnabled) videoBlue=m.getArgAsInt(0);
        if(m.getAddress() == "/svg/rotate/on") rotate3D=m.getArgAsBool(0);
        if(m.getAddress() == "/svg/rotate/x") rotateX=m.getArgAsBool(0);
        if(m.getAddress() == "/svg/rotate/y") rotateY=m.getArgAsBool(0);
        if(m.getAddress() == "/svg/rotate/z") rotateZ=m.getArgAsBool(0);
        if(m.getAddress() == "/svg/scale") svgScale=m.getArgAsFloat(0);
        if(m.getAddress() == "/zone/solo") soloZone(m.getArgAsInt(0),true);
    }
}

void ofApp::oscSendAllControls()
{
    //
    ofxOscMessage m;
    m.setAddress("svg/rotate/on");
    m.addBoolArg(rotate3D);
    oscSend.sendMessage(m);
    m.clear();
    m.setAddress("svg/rotate/x");
    m.addBoolArg(rotateX);
    oscSend.sendMessage(m);
    m.clear();
    m.setAddress("svg/rotate/y");
    m.addBoolArg(rotateY);
    oscSend.sendMessage(m);
    m.clear();
    m.setAddress("svg/rotate/z");
    m.addBoolArg(rotateZ);
    oscSend.sendMessage(m);
    m.clear();
    m.setAddress("svg/scale");
    m.addFloatArg(svgScale);
    oscSend.sendMessage(m);
    m.clear();


}
