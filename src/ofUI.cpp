#include "ofApp.h"

void ofApp::buildGui(){
    laserManager.drawLaserGui();

    ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGuiID dockspace_id = ImGui::GetID("DockSpace");
    static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_PassthruCentralNode;
    //static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

    ImGui::DockBuilderRemoveNode(dockspace_id); // clear any previous layout
    ImGui::DockBuilderAddNode(dockspace_id, dockspace_flags | ImGuiDockNodeFlags_DockSpace);
    ImGui::DockBuilderSetNodeSize(dockspace_id, viewport->Size);
/*
    ImGuiID dock_main_id = dockspace_id; // This variable will track the document node, however we are not using it here as we aren't docking anything into it.
    ImGuiID dock_id_prop = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Left, 0.20f, NULL, &dock_main_id);
    ImGuiID dock_id_bottom = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Down, 0.20f, NULL, &dock_main_id);

    auto dock_id_top = ImGui::DockBuilderSplitNode(dockspace_id, ImGuiDir_Up, 0.2f, nullptr, &dockspace_id);
    auto dock_id_down = ImGui::DockBuilderSplitNode(dockspace_id, ImGuiDir_Down, 0.25f, nullptr, &dockspace_id);
    auto dock_id_left = ImGui::DockBuilderSplitNode(dockspace_id, ImGuiDir_Left, 0.2f, nullptr, &dockspace_id);
    auto dock_id_right = ImGui::DockBuilderSplitNode(dockspace_id, ImGuiDir_Right, 0.15f, nullptr, &dockspace_id);
*/
    ImGui::DockBuilderDockWindow("General", dockspace_id);
    ImGui::DockBuilderDockWindow("MIDI/OSC", dockspace_id);
    ImGui::DockBuilderDockWindow("SVG Animation", dockspace_id);
    ImGui::DockBuilderDockWindow("SVG", dockspace_id);
    ImGui::DockBuilderDockWindow("LaserFX", dockspace_id);
    ImGui::DockBuilderDockWindow("Video input", dockspace_id);
    ImGui::DockBuilderFinish(dockspace_id);
    ImGui::DockSpace(dockspace_id);
    switch (beatIntervalSpeedIndex){
        case 1 :
        intervalSpeedString = "Speed: 64/1";
        beatIntervalSpeed = 384 ;
        break;
        case 2 :
        intervalSpeedString = "Speed: 32/1";
        beatIntervalSpeed = 192 ;
        break;
        case 3 :
        intervalSpeedString = "Speed: 16/1";
        beatIntervalSpeed = 96 ;
        break;
        case 4 :
        intervalSpeedString = "Speed: 8/1";
        beatIntervalSpeed = 48 ;
        break;
        case 5 :
        intervalSpeedString = "Speed: 1/4";
        beatIntervalSpeed = 24;
        break;
        case 6 :
        intervalSpeedString = "Speed: 1/8";
        beatIntervalSpeed = 12 ;
        break;
        case 7 :
        intervalSpeedString = "Speed: 1/16";
        beatIntervalSpeed = 6 ;
        break;
        case 8 :
        intervalSpeedString = "Speed: 1/32";
        beatIntervalSpeed = 3 ;
        break;
    }


    //General window
    //ImGui::SetNextWindowDockID(dock_id_prop);
    ofxLaser::UI::startWindow("General", ImVec2(1040,8), ImVec2(300 ,250));
    //ImGui::Begin("General");
    if(ImGui::Button("Load global settings")) {
        ofFileDialogResult openFileResult = ofSystemLoadDialog("Select folder with project jsons",true);
        if(openFileResult.bSuccess) {
            ofLog() << "load global settings";
        }
    }
    if(ImGui::Button("Save global settings")) {
        ofFileDialogResult openFileResult = ofSystemLoadDialog("Select folder with project jsons",true);
        if(openFileResult.bSuccess) {
            ofLog() << "load global settings";
        }
    }
    //SVGs




    ofxLaser::UI::endWindow();
    //ImGui::End();

    // MIDI window
    ofxLaser::UI::startWindow("MIDI/OSC", ImVec2(1040,260), ImVec2(300,200));
    if(ImGui::Button("start",ImVec2(40,20))) sequencer.start();
    ImGui::SameLine();
    if(ImGui::Button("stop",ImVec2(40,20))){
        sequencer.stop();
        sequencer.reset();
    }
    ImGui::SameLine();
    ImGui::PushItemWidth(100);
    ofxLaser::UI::addIntSlider(seqBPM);
    if(ImGui::Button("updateseq",ImVec2(20,20))) updateSequencerView();
    ImGui::Text(clockRunning ? "MIDI clock: running" : "MIDI clock: stopped");
    ImGui::SameLine();
    string bpmString = "bpm: "+ofToString(round(bpm));
    ImGui::Text(bpmString.c_str());
    int quarters = beats / 4; // convert total # beats to # quarters
    int bars = (quarters / 4) + 1; // compute # of bars
    int beatsInBar = (quarters % 4) + 1; // compute remainder as # notes within the current bar
    string barsBeats = "4/4 bars: "+ofToString(bars)+" beat: "+ofToString(beatsInBar);
    ImGui::SameLine();
    ImGui::Text(barsBeats.c_str());
    ofxLaser::UI::addParameter(svgFollowBeat);
    ofxLaser::UI::addParameter(seqSolo);
    ImGui::Text(intervalSpeedString.c_str());
    ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.65f);
    ofxLaser::UI::addIntSlider(beatIntervalSpeedIndex);
    if(ImGui::Button("Send OSC Controls")) oscSendAllControls();
    ofxLaser::UI::endWindow();

    //Animation window
    ofxLaser::UI::startWindow("SVG", ImVec2(1040,465), ImVec2(300,200));
    ofxLaser::UI::addParameter(svgEnabled);
    if(ImGui::Button("Select SVG folder")) {
        ofFileDialogResult openFileResult = ofSystemLoadDialog("Select folder containing SVGs",true);
        if(openFileResult.bSuccess) {
            string path = openFileResult.getPath();
            ofDirectory dir(path);
            dir.allowExt("svg");
            dir.listDir();
            dir.sort();
            // and load them all
            fileNames.clear();

            const vector<ofFile>& files = dir.getFiles();
            laserGraphics.clear();
            for(int i = 0; i<files.size();i++) {
                const ofFile & file = files.at(i);
                laserGraphics.emplace_back();
                // addSvgFromFile(string filename, bool optimise, bool subtractFills)
                laserGraphics.back().addSvgFromFile(file.getAbsolutePath(), false, true);
                // this centres all the graphics
                laserGraphics.back().autoCentre();
                ofLog(OF_LOG_NOTICE,file.getAbsolutePath());
                fileNames.push_back(file.getFileName());
            }
            dir.close();
        }
    }
    /*
    static int currentSvgDirIndex = 0;
    if(ofxImGui::VectorCombo("VectorCombo", &currentSvgDirIndex, fileNames))
    {
        ofLog() << "VectorCombo FILE PATH: "  << fileNames[currentSvgDirIndex].getAbsolutePath();
    }
*/
    ofxLaser::UI::addResettableFloatSlider(svgScale,1.0);
    ofxLaser::UI::addIntSlider(currentSVG);
    string filenameLabel = currentSVGFilename;
    ImGui::Text(filenameLabel.c_str());
    ImGui::Text("Rotate source image");
    ofxLaser::UI::addParameter(rotate3D);
    ofxLaser::UI::addParameter(rotateX);
    ofxLaser::UI::addParameter(rotateY);
    ofxLaser::UI::addParameter(rotateZ);
    ofxLaser::UI::addIntSlider(rotateSpeed);
    ofxLaser::UI::endWindow();

    //Laser FX window

    ofxLaser::UI::startWindow("LaserFX", ImVec2(1640,550), ImVec2(270,200));
    ofxLaser::UI::addParameter(fxEnabled);
    ImGui::Text(fxName.c_str());
    /*
    if(ImGui::Button("Load FX defaults")) {
        ofLog() << "Load fx defaults";
    }
    */
    if(ofxLaser::UI::addParameter(currentLaserEffect)) {
        //ofLog() << "FX changed" << currentLaserEffect;
        fxJustloaded = true;
        /*
        fxParamA.setName(fxParamALabel);
        fxParamB.setName(fxParamBLabel);
        fxParamC.setName(fxParamCLabel);
        fxParamD.setName(fxParamDLabel);
        fxParamE.setName(fxParamELabel);
        */
    }

    ofxLaser::UI::addParameter(fxLoadDefaults);
    ofxLaser::UI::addParameter(fxRed);
    ofxLaser::UI::addParameter(fxGreen);
    ofxLaser::UI::addParameter(fxBlue);
    ofxLaser::UI::addParameter(fxParamA);
    ofxLaser::UI::addParameter(fxParamB);
    ofxLaser::UI::addParameter(fxParamC);
    ofxLaser::UI::addParameter(fxParamD);
    ofxLaser::UI::addParameter(fxParamE);
    if(ImGui::Button("Trig time reset")) ofResetElapsedTimeCounter();
    ofxLaser::UI::endWindow();




    //Zone sequencer window
  //  ofxLaser::UI::startWindow("Zone sequencer", ImVec2(1350,660), ImVec2(280,200));
  //  ofxLaser::UI::addIntSlider(currentSolo);
  //  ofxLaser::UI::endWindow();

    //Video input window
    //
    //ImGui::Begin("Video input");
    ofxLaser::UI::startWindow("Video input",ImVec2(1350,550), ImVec2(280,200));
    ofxLaser::UI::addParameter(videoEnabled);
    static int videoDevIndex = 0;
    if(ofxImGui::VectorCombo("device", &videoDevIndex, videoDeviceNames)) selectedVideoDev = videoDevices[videoDevIndex].id;
    if(ImGui::Button("add video source")) videoInputAdd(selectedVideoDev);
    if(ImGui::Button("reset contour")) resetContour();
    ofxLaser::UI::addParameter(videoRed);
    ofxLaser::UI::addParameter(videoGreen);
    ofxLaser::UI::addParameter(videoBlue);
    for (int i = 0; i < numVideoInputs ; i ++){
        string s = to_string(i);
        char const *tab_label = s.c_str();
        ImGui::BeginTabBar("videoInTabs");
        if (ImGui::BeginTabItem(tab_label)){
            //if(ImGui::Button("Start video")) videoStart();
            //if(ImGui::Button("Stop video")) videoStop();
            ImGui::Text("contour settings");
            ofxLaser::UI::addIntSlider(videoInputs[i].contourThreshold);
            ofxLaser::UI::addFloatSlider(videoInputs[i].contourSimplify);
            ofxLaser::UI::addFloatSlider(videoInputs[i].contourResample);
            ofxLaser::UI::addFloatSlider(videoInputs[i].rotateDeg);
            //if(ImGui::Button("Reset anchor")) vidcontour.resetAnchor();
            ImGui::EndTabItem();
        }

        ImGui::EndTabBar();
    }
    ofxLaser::UI::endWindow();
    //ImGui::End();





    laserManager.finishLaserUI();
}
