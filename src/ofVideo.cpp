#include "ofApp.h"

int ofApp::videoGetDevices(){
    //get back a list of devices.
    videoDevices = vidGrabber.listDevices();
    for(size_t i = 0; i < videoDevices.size(); i++){
        if(videoDevices[i].bAvailable){
         //log the device
         ofLogNotice() << videoDevices[i].id << ": " << videoDevices[i].deviceName;
         videoDeviceNames.push_back(videoDevices[i].deviceName);
        }else{
            //log the device and note it as unavailable
         ofLogNotice() << videoDevices[i].id << ": " << videoDevices[i].deviceName << " - unavailable ";
        }
    }
    return 0;

}

int ofApp::videoCleanup(){
    for (int i=0; i < numVideoInputs; i++){
        videoInputs[i].grabber.close();
        videoInputs[i].videoEnabled = false;
    }
    return 0;
}

int ofApp::videoInputAdd(int deviceNum){
    ofSetVerticalSync(true);
    videoInputs.push_back(videoInput());
    videoInputs[numVideoInputs].grabber.setDeviceID(deviceNum);
    videoInputs[numVideoInputs].grabber.setDesiredFrameRate(30);
    videoInputs[numVideoInputs].grabber.setup(640,480);
    videoInputs[numVideoInputs].colorImage.allocate(videoInputs[numVideoInputs].grabber.getWidth(),videoInputs[numVideoInputs].grabber.getHeight());
    videoInputs[numVideoInputs].grayImage.allocate(videoInputs[numVideoInputs].grabber.getWidth(),videoInputs[numVideoInputs].grabber.getHeight());
    videoInputs[numVideoInputs].background.allocate(videoInputs[numVideoInputs].grabber.getWidth(),videoInputs[numVideoInputs].grabber.getHeight());
    videoInputs[numVideoInputs].difference.allocate(videoInputs[numVideoInputs].grabber.getWidth(),videoInputs[numVideoInputs].grabber.getHeight());
    videoInputs[numVideoInputs].contourThreshold.set("Threshhold", 20, 1, 256);
    videoInputs[numVideoInputs].contourSimplify.set("Simplify", 1.5, 0.1, 10.0);
    videoInputs[numVideoInputs].contourResample.set("Resample", 10, 1, 100);
    videoInputs[numVideoInputs].rotateDeg.set("Rotate",0,0,359);
    videoInputs[numVideoInputs].videoEnabled = true;
    string video = "video";
    string num = to_string(deviceNum);
    string label = video + num;
    videoInputs[numVideoInputs].label = label;
    numVideoInputs++;




    return 0;


}
void ofApp::resetContour(){
    videoGraphic.clear();
    for(int i = 0 ; i<videoInputs.size() ; i++){
        videoInputs[i].contour.resetAnchor();
    }

}

/*void drawVideoInputs(){
    for(int i; i < videoInputs.size() ; i ++){

    }
}
*/
/*
int ofApp::videoStart(){
  //  ofLog() << "start video";
    // v4l2
    videoWidth = 640;  // try to grab at this size.
    videoHeight = 480;

    vidGrabber.setDeviceID(selectedVideoDev);
    vidGrabber.setDesiredFrameRate(20);
    vidGrabber.initGrabber(videoWidth, videoHeight);
   // videoInverted.allocate(videoWidth, videoHeight, OF_PIXELS_RGB);
  //  videoTexture.allocate(videoInverted);
    ofSetVerticalSync(true);
    colorImage.allocate(vidGrabber.getWidth(),vidGrabber.getHeight());
    grayImage.allocate(vidGrabber.getWidth(),vidGrabber.getHeight());
    background.allocate(vidGrabber.getWidth(),vidGrabber.getHeight());
    difference.allocate(vidGrabber.getWidth(),vidGrabber.getHeight());

    videoEnabled = true;
    return 0;
}

int ofApp::videoStop(){
  //  ofLog() << "stop video";
    videoEnabled = false;
    return 0;
}
*/
void ofApp::test_videoUpdate(){
    if (contourResetScheduler.reset == true){
      resetContour();
      contourResetScheduler.reset = false;
    }

   // resetContour();
    ofPolyline tempPoly;
    ofPolyline tempPolyResampled;
    ofColor c;
    c.r=videoRed;
    c.g=videoGreen;
    c.b=videoBlue;
    for(int i = 0 ; i<videoInputs.size() ; i++){
        videoInputs[i].grabber.update();
        if(videoInputs[i].grabber.isFrameNew()){
            videoInputs[i].colorImage.setFromPixels(videoInputs[i].grabber.getPixels());
            videoInputs[i].grayImage = videoInputs[i].colorImage;
            if(videoInputs[i].learn){
                videoInputs[i].background = videoInputs[i].grayImage;
                videoInputs[i].learn = false;
            }
            videoInputs[i].difference.absDiff(videoInputs[i].background,videoInputs[i].grayImage);
            videoInputs[i].difference.threshold(videoInputs[i].contourThreshold);
            int num = videoInputs[i].contour.findContours(videoInputs[i].difference , 5 , videoInputs[i].grabber.getWidth()*videoInputs[i].grabber.getHeight() , 10 , false );

            if(num>=1){
                videoGraphic.clear();
                for(int y = 0; y <  videoInputs[i].contour.nBlobs; y++){
                    tempPoly.addVertices(videoInputs[i].contour.blobs[y].pts);
                    tempPoly.simplify(videoInputs[i].contourSimplify);
                    tempPolyResampled = tempPoly.getResampledBySpacing(videoInputs[i].contourResample);
                    for(int i=0; i < tempPolyResampled.size(); i++){

                    }

                    videoGraphic.addPolyline(tempPolyResampled,c,false,false);



                    tempPoly.clear();
                    tempPolyResampled.clear();
                }
                videoInputs[i].cvCur.close();
                videoInputs[i].cvCur.simplify(videoInputs[i].contourSimplify);
                videoInputs[i].cvPoly.clear();
                videoInputs[i].cvPoly = videoInputs[i].cvCur.getResampledBySpacing(videoInputs[i].contourResample);
            }
        }
    }
}


/*
void ofApp::videoUpdate(){

    if(videoEnabled){
        vidGrabber.update();
        if(vidGrabber.isFrameNew()){
            colorImage.setFromPixels(vidGrabber.getPixels());
            grayImage = colorImage;
            if(learn){
                background = grayImage;
                learn = false;
            }
            difference.absDiff(background,grayImage);
            difference.threshold(contourThreshold);
            int num = contour.findContours(difference , 10 , vidGrabber.getWidth()*vidGrabber.getHeight() , 10 , true );
            if(num>=1){
               // for (int i; i < contour.blobs.size(); i++){
                    //contourPolylines.emplace_back();
                    //ofLog() << "numblobs:" << contour.blobs.size();
                    cvCur.clear();
                    cvCur.addVertices(contour.blobs[0].pts);
                    //contourPolylines.back().addPolyline(cvCur,c,false,true);
                    cvCur.close();
                    cvCur.simplify(contourSimplify);
                    cvPoly.clear();
                    cvPoly = cvCur.getResampledBySpacing(contourResample);
                    ofLog() << "ofpoly size: " << cvPoly.size();
            }
        }
    }
}
*/
